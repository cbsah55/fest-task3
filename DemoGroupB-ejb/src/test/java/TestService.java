import com.admin.dto.BookDto;
import com.admin.service.BookService;
import com.admin.serviceImpl.BookServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;

import javax.persistence.EntityManager;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class TestService {
    EntityManager em;
    private BookService bookDao;
@BeforeEach
void setup(){
    bookDao = new BookServiceImpl();


}

    @Test
    public void addBookServiceTest(){
        BookDto bookDto = new BookDto();
        bookDto.setName("Science");
        bookDto.setDescription("this is from test");
        assertTrue(bookDao.addBook(bookDto));

    }
}
