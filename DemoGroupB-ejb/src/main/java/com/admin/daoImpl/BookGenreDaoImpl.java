package com.admin.daoImpl;

import com.admin.dao.BookGenreDao;
import com.payrollSystem.entity.common.BookGenre;

import javax.ejb.Stateless;

@Stateless
public class BookGenreDaoImpl extends StatusableDaoImpl<BookGenre> implements BookGenreDao {
    public BookGenreDaoImpl(){
        super(BookGenre.class);
    }
}
