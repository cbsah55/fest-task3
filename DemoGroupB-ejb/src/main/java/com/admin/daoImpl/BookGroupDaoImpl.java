package com.admin.daoImpl;

import com.admin.dao.BookGroupDao;
import com.payrollSystem.entity.common.BookGroup;

import javax.ejb.Stateless;

@Stateless
public class BookGroupDaoImpl extends StatusableDaoImpl<BookGroup> implements BookGroupDao {
    public BookGroupDaoImpl(){
        super(BookGroup.class);
    }
}
