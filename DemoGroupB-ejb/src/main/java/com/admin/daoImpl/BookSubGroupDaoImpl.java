package com.admin.daoImpl;

import com.admin.dao.BookSubGroupDao;
import com.payrollSystem.entity.common.BookSubGroup;

import javax.ejb.Stateless;

@Stateless
public class BookSubGroupDaoImpl extends StatusableDaoImpl<BookSubGroup> implements BookSubGroupDao {
    public BookSubGroupDaoImpl(){
        super(BookSubGroup.class);
    }
}
