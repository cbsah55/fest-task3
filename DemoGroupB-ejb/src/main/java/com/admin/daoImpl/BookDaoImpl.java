package com.admin.daoImpl;

import com.admin.dao.BookDao;
import com.payrollSystem.entity.common.BookCategory;

import javax.ejb.Stateless;
import java.util.Optional;

@Stateless
public class BookDaoImpl extends StatusableDaoImpl<BookCategory> implements BookDao {

public BookDaoImpl(){
    super(BookCategory.class);
}
    @Override
    public BookCategory getByBookName(String bookName) {

        return (BookCategory) Optional.ofNullable( getEntityManager()
                .createQuery("select  bc from "+getPersistenceClass().getSimpleName()+" bc where bc.name =:NAME")
                .setParameter("NAME",bookName)
                .getSingleResult()).orElse(null);
    }
}
