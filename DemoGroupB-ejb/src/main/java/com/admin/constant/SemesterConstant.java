package com.admin.constant;

import java.util.Arrays;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum SemesterConstant {
    One(1),
    Two(2),
    Three(3),
    Four(4);
    private final Integer semester;
    
    public static List<Integer> semesterList(){
        return Arrays.asList(One.getSemester(),Two.getSemester(),Three.getSemester(),Four.getSemester());
    }

}
