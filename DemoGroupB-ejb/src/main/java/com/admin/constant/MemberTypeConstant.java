package com.admin.constant;

import java.util.Arrays;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum MemberTypeConstant {
    Student( "Student"),
    Staff("Staff");
    private final String name;
    
    public static List<String> membersList(){
        return Arrays.asList(Student.getName(),Staff.getName());
    }

}
