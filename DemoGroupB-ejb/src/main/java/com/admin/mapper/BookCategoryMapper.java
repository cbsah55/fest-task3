package com.admin.mapper;

import com.admin.dto.BookDto;
import com.payrollSystem.entity.common.BookCategory;


public class BookCategoryMapper extends AbstractProfileMapper{
    public static BookDto convertToBookDto(BookCategory bookCategory){
        BookDto bookDto = new BookDto();
        convertCommonParameters(bookDto,bookCategory);
        return bookDto;
    }
}
