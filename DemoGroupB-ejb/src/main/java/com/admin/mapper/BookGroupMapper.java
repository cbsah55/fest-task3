package com.admin.mapper;

import com.admin.dto.BookGroupDto;
import com.payrollSystem.entity.common.BookGroup;

public class BookGroupMapper extends AbstractProfileMapper {

    public static BookGroupDto convertToDto(BookGroup bookGroup){
        BookGroupDto bookGroupDto = new BookGroupDto();
        convertCommonParameters(bookGroupDto,bookGroup);
        return bookGroupDto;
    }

}
