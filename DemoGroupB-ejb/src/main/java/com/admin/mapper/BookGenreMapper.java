package com.admin.mapper;

import com.admin.dto.BookGenreDto;
import com.payrollSystem.entity.common.BookGenre;

public class BookGenreMapper extends AbstractProfileMapper {

    public static BookGenreDto convertToBookGenreDto(BookGenre bookGenre){
        BookGenreDto bookGenreDto = new BookGenreDto();
        convertCommonParameters(bookGenreDto,bookGenre);
        return bookGenreDto;
    }
}
