package com.admin.service;

import com.admin.dto.BookGenreDto;

import javax.ejb.Local;
import java.util.List;

@Local
public interface BookGenreService {
    boolean addBookGenre(BookGenreDto bookGenreDto);
    boolean updateBookGenre(BookGenreDto bookGenreDto);
    boolean deleteBookGenre(BookGenreDto bookGenreDto);
    List<BookGenreDto> getAllBookGenres();
}
