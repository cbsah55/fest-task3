package com.admin.service;

import com.admin.dto.BookGroupDto;
import com.admin.dto.BookSubGroupDto;
import com.payrollSystem.entity.common.BookGroup;

import javax.ejb.Local;
import java.util.List;

@Local
public interface BookSubGroupService{

    boolean createBookSubGroup(BookSubGroupDto bookSubGroupDto);
    boolean updateBookSubGroup(BookSubGroupDto bookSubGroupDto);
    boolean deleteBookSubGroup(BookSubGroupDto bookSubGroupDto);
    List<BookSubGroupDto> findByBookGroupId(BookGroup bookGroup);
    List<BookSubGroupDto> findAllBookSubGroup();
    List<BookGroupDto> findBookGroupsForDropDown();
}
