package com.admin.service;

import com.admin.dto.BookGroupDto;

import javax.ejb.Local;
import java.util.List;

@Local
public interface BookGroupService {
    boolean addBookGroup(BookGroupDto bookGroupDto);
    boolean updateBookGroup(BookGroupDto bookGroupDto);
    boolean deleteBookGroup(BookGroupDto bookGroupDto);
    BookGroupDto findBookGroupById(long id);
    List<BookGroupDto> findAllBookGroups();
}
