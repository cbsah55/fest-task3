package com.admin.service;

import com.admin.dto.BookDto;

import javax.ejb.Local;
import java.util.List;
@Local
public interface BookService {
    boolean addBook(BookDto bookDto);
    boolean updateBook(BookDto bookDto);
    boolean deleteBook(BookDto bookDto);
    BookDto getBook(long id);
     List<BookDto> getAllBooks();
}
