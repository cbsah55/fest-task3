package com.admin.service;

import com.admin.dto.BookDto;
import com.admin.dto.IssueRuleSettingDto;
import com.payrollSystem.entity.common.BookCategory;
import com.payrollSystem.entity.common.IssueRuleSetting;

import javax.ejb.Local;
import java.util.List;

@Local
public interface IssueRuleSettingService {
    boolean createIssueRuleSetting(IssueRuleSettingDto issueRuleSettingDto);
    boolean updateIssueRuleSetting(IssueRuleSettingDto issueRuleSettingDto);
    boolean deleteIssueRuleSetting(IssueRuleSettingDto issueRuleSettingDto);
    boolean checkIfMemberAlreadyExists(IssueRuleSettingDto issueRuleSettingDto);
    boolean checkIfSemesterAlreadyExists(IssueRuleSettingDto issueRuleSettingDto);
    boolean checkIfBookCategoryAlreadyExists(IssueRuleSettingDto issueRuleSettingDto);
    List<IssueRuleSettingDto> getAllIssueRuleSetting();
    boolean checkIfIssueSettingAlreadyExists(IssueRuleSettingDto issueRuleSettingDto);
    List<BookDto> getBookCategoryForDropdown();
}
