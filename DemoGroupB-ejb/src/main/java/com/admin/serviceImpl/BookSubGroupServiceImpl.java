package com.admin.serviceImpl;

import com.admin.constant.StatusConstants;
import com.admin.dao.AdminDao;
import com.admin.dao.BookGroupDao;
import com.admin.dao.BookSubGroupDao;
import com.admin.dao.StatusDao;
import com.admin.dto.BookGroupDto;
import com.admin.dto.BookSubGroupDto;
import com.admin.mapper.BookGroupMapper;
import com.admin.mapper.BookSubGroupMapper;
import com.admin.service.BookSubGroupService;
import com.payrollSystem.entity.common.BookGroup;
import com.payrollSystem.entity.common.BookSubGroup;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Stateless
public class BookSubGroupServiceImpl implements BookSubGroupService {
    @EJB
    private BookSubGroupDao bookSubGroupDao;
    @EJB
    private StatusDao statusDao;
    @EJB
    private BookGroupDao bookGroupDao;
    @EJB
    private AdminDao adminDao;

    @Override
    public boolean createBookSubGroup(BookSubGroupDto bookSubGroupDto) {
        return bookSubGroupDao.save(this.convertToBookSubGroup(bookSubGroupDto));
    }

    @Override
    public boolean updateBookSubGroup(BookSubGroupDto bookSubGroupDto) {
        BookSubGroup bookSubGroup = bookSubGroupDao.getById(bookSubGroupDto.getId());
        bookSubGroup.setLastUpdatedDate(new Date());
        bookSubGroup.setUpdatedByAdmin(adminDao.getById(bookSubGroupDto.getUpdatedByAdminDto().getId()));
        bookSubGroup.setStatus(statusDao.getByDesc(StatusConstants.EDIT_APPROVE.getName()));
        bookSubGroup.setBookGroup(bookGroupDao.getById(bookSubGroupDto.getBookGroupDto().getId()));
       setEditedCommonParameters(bookSubGroup,bookSubGroupDto);
       return bookSubGroupDao.modify(bookSubGroup);

    }

    @Override
    public boolean deleteBookSubGroup(BookSubGroupDto bookSubGroupDto) {
        BookSubGroup bookSubGroup = bookSubGroupDao.getById(bookSubGroupDto.getId());
        bookSubGroup.setDeletedDate(new Date());
        bookSubGroup.setDeletedByAdmin(adminDao.getById(bookSubGroupDto.getDeletedByAdminDto().getId()));
        bookSubGroup.setDeletedReason(bookSubGroupDto.getDeletedReason());
        bookSubGroup.setStatus(statusDao.getByDesc(StatusConstants.DELETED_APPROVE.getName()));
        return bookSubGroupDao.modify(bookSubGroup);
    }

    private void setEditedCommonParameters(BookSubGroup bookSubGroup,BookSubGroupDto bookSubGroupDto) {
        bookSubGroup.setName(bookSubGroupDto.getName());
        bookSubGroup.setDescription(bookSubGroupDto.getDescription());
    }

    @Override
    public List<BookSubGroupDto> findByBookGroupId(BookGroup bookGroup) {
        // returns empty because method to find booksubgroup by bookgroup is not implemented in dao
        //if you want to implement, create a method in BookSubGroupDao
        return Collections.emptyList();
    }

    @Override
    public List<BookSubGroupDto> findAllBookSubGroup() {
        return bookSubGroupDao.findAll().stream().map(BookSubGroupMapper::convertToDto).collect(Collectors.toList());
    }

    @Override
    public List<BookGroupDto> findBookGroupsForDropDown() {
        return bookGroupDao.findAll().stream().map(BookGroupMapper::convertToDto).collect(Collectors.toList());
    }

    protected BookSubGroup convertToBookSubGroup(BookSubGroupDto bookSubGroupDto){
        BookSubGroup bookSubGroup = new BookSubGroup();
        bookSubGroup.setName(bookSubGroupDto.getName());
        bookSubGroup.setDescription(bookSubGroupDto.getDescription());
        bookSubGroupDto.setCreatedDate(new Date());
        bookSubGroup.setCreatedDate(bookSubGroupDto.getCreatedDate());
        bookSubGroup.setLastUpdatedDate(bookSubGroupDto.getLastUpdatedDate());
        bookSubGroup.setDeletedDate(bookSubGroupDto.getDeletedDate());
        bookSubGroup.setStatus(statusDao.getByDesc(StatusConstants.CREATE_APPROVE.getName()));
        bookSubGroup.setBookGroup(bookGroupDao.getById(bookSubGroupDto.getBookGroupDto().getId()));
        bookSubGroup.setCreatedByAdmin(adminDao.getById(bookSubGroupDto.getCreatedByAdminDto().getId()));
        return bookSubGroup;
    }
}
