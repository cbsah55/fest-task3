package com.admin.serviceImpl;

import com.admin.constant.StatusConstants;
import com.admin.dao.AdminDao;
import com.admin.dao.BookGenreDao;
import com.admin.dao.StatusDao;
import com.admin.dto.BookGenreDto;
import com.admin.mapper.BookGenreMapper;
import com.admin.service.BookGenreService;
import com.payrollSystem.entity.common.BookGenre;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Stateless
public class BookGenreServiceImpl implements BookGenreService {
    @EJB
    private BookGenreDao bookGenreDao;
    @EJB
    private AdminDao adminDao;
    @EJB
    private StatusDao statusDao;


    @Override
    public boolean addBookGenre(BookGenreDto bookGenreDto) {
        bookGenreDto.setCreatedDate(new Date());
        return bookGenreDao.save(convertToBookSubGroup(bookGenreDto));
    }

    @Override
    public boolean updateBookGenre(BookGenreDto bookGenreDto) {
        BookGenre bookGenre = bookGenreDao.getById(bookGenreDto.getId());
        bookGenre.setLastUpdatedDate(new Date());
        bookGenre.setUpdatedByAdmin(adminDao.getById(bookGenreDto.getUpdatedByAdminDto().getId()));
        bookGenre.setStatus(statusDao.getByDesc(StatusConstants.EDIT_APPROVE.getName()));
        setEditedCommonParameters(bookGenre,bookGenreDto);
        return bookGenreDao.modify(bookGenre);
    }

    @Override
    public boolean deleteBookGenre(BookGenreDto bookGenreDto) {
        BookGenre bookGenre = bookGenreDao.getById(bookGenreDto.getId());
        bookGenre.setDeletedDate(new Date());
        bookGenre.setDeletedByAdmin(adminDao.getById(bookGenreDto.getDeletedByAdminDto().getId()));
        bookGenre.setDeletedReason(bookGenreDto.getDeletedReason());
        bookGenre.setStatus(statusDao.getByDesc(StatusConstants.DELETED_APPROVE.getName()));
        return false;
    }

    @Override
    public List<BookGenreDto> getAllBookGenres() {
        return bookGenreDao.findAll().stream().map(BookGenreMapper::convertToBookGenreDto).collect(Collectors.toList());
    }

    private void setEditedCommonParameters(BookGenre bookGenre,BookGenreDto bookGenreDto) {
        bookGenre.setName(bookGenreDto.getName());
        bookGenre.setDescription(bookGenreDto.getDescription());
    }

    protected BookGenre convertToBookSubGroup(BookGenreDto bookGenreDto){
        BookGenre bookGenre = new BookGenre();
        bookGenre.setName(bookGenreDto.getName());
        bookGenre.setDescription(bookGenreDto.getDescription());
        bookGenre.setCreatedDate(bookGenreDto.getCreatedDate());
        bookGenre.setLastUpdatedDate(bookGenreDto.getLastUpdatedDate());
        bookGenre.setDeletedDate(bookGenreDto.getDeletedDate());
        bookGenre.setCreatedByAdmin(adminDao.getById(bookGenreDto.getCreatedByAdminDto().getId()));
        bookGenre.setStatus(statusDao.getByDesc(StatusConstants.CREATE_APPROVE.getName()));
        return bookGenre;
    }
}
