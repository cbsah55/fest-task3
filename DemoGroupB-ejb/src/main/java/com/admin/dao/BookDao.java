package com.admin.dao;

import com.payrollSystem.entity.common.BookCategory;

import javax.ejb.Local;

@Local
public interface BookDao extends StatusableDao<BookCategory> {

    BookCategory getByBookName(String bookName);

}
