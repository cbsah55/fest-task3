package com.admin.dao;

import com.payrollSystem.entity.common.BookGenre;

import javax.ejb.Local;

@Local
public interface BookGenreDao  extends StatusableDao<BookGenre> {
}
