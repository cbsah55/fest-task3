package com.admin.dao;

import com.admin.dto.IssueRuleSettingDto;
import com.payrollSystem.entity.common.IssueRuleSetting;

import javax.ejb.Local;

@Local
public interface IssueRuleSettingDao  extends StatusableDao<IssueRuleSetting> {
    boolean checkIfMemberTypeAlreadyExists(IssueRuleSettingDto issueRuleSettingDto);
    boolean checkIfSemesterAlreadyExists(IssueRuleSettingDto issueRuleSettingDto);
    boolean checkIfBookCategoryAlreadyExists(IssueRuleSettingDto issueRuleSettingDto);
}
