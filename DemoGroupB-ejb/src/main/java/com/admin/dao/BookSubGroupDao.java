package com.admin.dao;

import com.payrollSystem.entity.common.BookSubGroup;

import javax.ejb.Local;

@Local
public interface BookSubGroupDao extends StatusableDao<BookSubGroup> {
}
