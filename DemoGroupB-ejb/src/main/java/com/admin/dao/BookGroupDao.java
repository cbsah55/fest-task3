package com.admin.dao;

import com.payrollSystem.entity.common.BookGroup;

import javax.ejb.Local;

@Local
public interface BookGroupDao extends StatusableDao<BookGroup> {
}
