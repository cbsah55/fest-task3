package com.admin.dto;

import com.admin.dto.abstracts.AbstractCodeDto;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class BookDto extends AbstractCodeDto {

}
