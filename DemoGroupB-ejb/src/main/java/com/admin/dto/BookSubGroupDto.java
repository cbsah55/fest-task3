package com.admin.dto;

import com.admin.dto.abstracts.AbstractCodeDto;
import com.payrollSystem.entity.common.BookGroup;
import lombok.Getter;
import lombok.Setter;

import java.util.Optional;

@Getter
@Setter
public class BookSubGroupDto extends AbstractCodeDto {
    private BookGroupDto bookGroupDto;

    public BookSubGroupDto(){
        this.bookGroupDto = Optional.ofNullable(bookGroupDto).orElse(new BookGroupDto());
    }
}
