package com.admin.dto;

import com.admin.dto.abstracts.AbstractCodeDto;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class BookGenreDto extends AbstractCodeDto {
}
