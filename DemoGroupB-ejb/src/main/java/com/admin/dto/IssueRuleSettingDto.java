package com.admin.dto;

import com.admin.dto.abstracts.AbstractStatusHelperDto;
import com.payrollSystem.entity.common.BookCategory;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.util.Optional;

@Getter
@Setter
public class IssueRuleSettingDto extends AbstractStatusHelperDto {
    
    private String memberType;
    
    private int semester;
    
    private BookDto bookCategoryDto;
    
    private int noOfBookAllowed;
    
    private int noOfRenews;
    
    private int noOfRenewalDays;

    private double finePerExtraDay;

    public IssueRuleSettingDto (){
        this.bookCategoryDto = Optional.ofNullable(bookCategoryDto).orElse(new BookDto());
    }

}
