package com.admin.bookGroup;

import com.admin.dto.BookGroupDto;
import lombok.Getter;
import lombok.Setter;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import java.io.Serializable;
import java.util.List;
import java.util.Optional;

@Getter
@Setter
@ManagedBean
@SessionScoped
public class BookGroupDataModelJsf implements Serializable {
    private BookGroupDto bookGroupDto;
    private boolean edit;
    private List<BookGroupDto> bookGroupDtos;

    public BookGroupDto getBookGroupDto(){
        return Optional.ofNullable(bookGroupDto).orElse(new BookGroupDto());
    }
}
