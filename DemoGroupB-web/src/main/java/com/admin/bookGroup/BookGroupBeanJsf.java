package com.admin.bookGroup;

import com.admin.dto.AdminDto;
import com.admin.dto.BookGroupDto;
import com.admin.service.BookGroupService;
import com.admin.util.Utility;
import lombok.Getter;
import lombok.Setter;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import java.io.Serializable;

@Getter
@Setter
@ManagedBean
@RequestScoped
public class BookGroupBeanJsf implements Serializable {

    @ManagedProperty(value = "#{bookGroupDataModelJsf}")
    private BookGroupDataModelJsf bookGroupDataModelJsf;

    @EJB
    private BookGroupService bookGroupService;
    private AdminDto adminDto;
    @PostConstruct
    public void init(){
        adminDto = new AdminDto();
        adminDto.setId(1L);
    }


    public String save(){
            boolean success = bookGroupService.addBookGroup(bookGroupDataModelJsf.getBookGroupDto());
        if (success) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Successfully Saved", null));
        }
        return navigateToPage();
    }
    public String update(){
        boolean response = bookGroupService.updateBookGroup(bookGroupDataModelJsf.getBookGroupDto());
        if (response) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Updated Successfully", null));
        }
        return navigateToPage();
    }
    public String delete(){
        boolean success = bookGroupService.deleteBookGroup(bookGroupDataModelJsf.getBookGroupDto());
        if (success){
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Deleted Successfully", null));
        }
        return navigateToPage();
    }
    public String saveUpdate(){
        bookGroupDataModelJsf.getBookGroupDto().setCreatedByAdminDto(adminDto);
        bookGroupDataModelJsf.getBookGroupDto().setUpdatedByAdminDto(adminDto);
        return bookGroupDataModelJsf.getBookGroupDto().getId() == null ? save():update();

    }
    public String initCreate(){
        bookGroupDataModelJsf.setBookGroupDto(new BookGroupDto());
        bookGroupDataModelJsf.setEdit(true);
        return returnToPage();
    }

    public String returnToPage() {
        return "bookGroup.xhtml?faces-redirect=true";
    }
    public String navigateToPage(){
        Utility.removeSessionBeanJSFDataModelObject("bookGroupDataModelJsf");
        bookGroupDataModelJsf = (BookGroupDataModelJsf) Utility.getSessionObject("bookGroupDataModelJsf");
        bookGroupDataModelJsf.setBookGroupDtos(bookGroupService.findAllBookGroups());
        return returnToPage();
    }

    public String initEdit(){
        bookGroupDataModelJsf.setEdit(true);
        return returnToPage();
    }
}
