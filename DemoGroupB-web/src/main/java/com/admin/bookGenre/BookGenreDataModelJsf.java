package com.admin.bookGenre;

import com.admin.dto.BookGenreDto;
import lombok.Getter;
import lombok.Setter;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import java.io.Serializable;
import java.util.List;
import java.util.Optional;

@Getter
@Setter
@ManagedBean
@SessionScoped
public class BookGenreDataModelJsf implements Serializable {
    private BookGenreDto bookGenreDto;
    private boolean isEdit;
    private List<BookGenreDto> bookGenreDtos;

    public BookGenreDto getBookGenreDto(){
        return Optional.ofNullable(bookGenreDto).orElse(new BookGenreDto());
    }
}
