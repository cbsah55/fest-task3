package com.admin.bookGenre;

import com.admin.bookSubGroup.BookSubGroupDataModelJsf;
import com.admin.dto.AdminDto;
import com.admin.dto.BookGenreDto;
import com.admin.dto.BookSubGroupDto;
import com.admin.service.BookGenreService;
import com.admin.util.Utility;
import lombok.Getter;
import lombok.Setter;
import lombok.var;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;


@Getter
@Setter
@ManagedBean
@RequestScoped
public class BookGenreBeanJsf  {

    @ManagedProperty(value = "#{bookGenreDataModelJsf}")
    private BookGenreDataModelJsf bookGenreDataModelJsf;

    @EJB
    private BookGenreService bookGenreService;

    private AdminDto adminDto;

    @PostConstruct
    public void init(){
        adminDto = new AdminDto();
        adminDto.setId(1L);
    }

    public  String save(){
        bookGenreDataModelJsf.getBookGenreDto().setCreatedByAdminDto(adminDto);
        var response = bookGenreService.addBookGenre(bookGenreDataModelJsf.getBookGenreDto());
        if (response)
            FacesContext.getCurrentInstance()
                    .addMessage(null,
                            new FacesMessage(FacesMessage.SEVERITY_INFO,"Saved Successfully",null));
        return navigateToPage();
    }

    public String update(){
        bookGenreDataModelJsf.getBookGenreDto().setUpdatedByAdminDto(adminDto);
        var response = bookGenreService.updateBookGenre(bookGenreDataModelJsf.getBookGenreDto());
        if (response)
            FacesContext.getCurrentInstance()
                    .addMessage(null,
                            new FacesMessage(FacesMessage.SEVERITY_INFO,"Saved Successfully",null));
        return navigateToPage();
    }

    public String saveUpdate(){
        return bookGenreDataModelJsf.getBookGenreDto().getId() == null? save():update();

    }

    public String delete(){
        bookGenreDataModelJsf.getBookGenreDto().setDeletedByAdminDto(adminDto);
        var response = bookGenreService.deleteBookGenre(bookGenreDataModelJsf.getBookGenreDto());
        if (response)
            FacesContext.getCurrentInstance()
                    .addMessage(null,
                            new FacesMessage(FacesMessage.SEVERITY_INFO,"Saved Successfully",null));
        return navigateToPage();

    }

    public String initCreate(){
        bookGenreDataModelJsf.setBookGenreDto(new BookGenreDto());
        bookGenreDataModelJsf.setEdit(true);
        return returnToPage();
    }
    public String initEdit(){
        bookGenreDataModelJsf.setEdit(true);
        return returnToPage();
    }
    public String returnToPage(){
        return "bookGenre.xhtml?faces-redirect=true";
    }
    public String navigateToPage(){
        Utility.removeSessionBeanJSFDataModelObject("bookGenreDataModelJsf");
        bookGenreDataModelJsf = (BookGenreDataModelJsf) Utility.getSessionObject("bookGenreDataModelJsf");
        bookGenreDataModelJsf.setBookGenreDtos(bookGenreService.getAllBookGenres());
        return returnToPage();
    }
}
