package com.admin.bookSubGroup;

import com.admin.dto.BookGroupDto;
import com.admin.dto.BookSubGroupDto;
import lombok.Getter;
import lombok.Setter;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import java.io.Serializable;
import java.util.List;
import java.util.Optional;

@Getter
@Setter
@ManagedBean
@SessionScoped
public class BookSubGroupDataModelJsf implements Serializable {
    private BookSubGroupDto bookSubGroupDto;
    private List<BookGroupDto> bookGroupDtoListForDropDown;
    private List<BookSubGroupDto> bookSubGroupDtoList;
    private boolean initEdit;

    public BookSubGroupDto getBookSubGroupDto(){
        return Optional.ofNullable(bookSubGroupDto).orElse(new BookSubGroupDto());
    }
}
