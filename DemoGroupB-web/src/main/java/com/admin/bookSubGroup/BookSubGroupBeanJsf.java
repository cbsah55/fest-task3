package com.admin.bookSubGroup;

import com.admin.dto.AdminDto;
import com.admin.dto.BookSubGroupDto;
import com.admin.service.BookSubGroupService;
import com.admin.util.Utility;
import lombok.Getter;
import lombok.Setter;
import lombok.var;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import java.io.Serializable;

@Getter
@Setter
@ManagedBean
@RequestScoped
public class BookSubGroupBeanJsf implements Serializable {

    @ManagedProperty(value = "#{bookSubGroupDataModelJsf}")
    private BookSubGroupDataModelJsf bookSubGroupDataModelJsf;

    @EJB
    private BookSubGroupService bookSubGroupService;
    private AdminDto adminDto;
    @PostConstruct
    public void init(){
        adminDto = new AdminDto();
        adminDto.setId(1L);
    }

    public String save(){
        bookSubGroupDataModelJsf.getBookSubGroupDto().setCreatedByAdminDto(adminDto);
        var response = bookSubGroupService.createBookSubGroup(bookSubGroupDataModelJsf.getBookSubGroupDto());
        if (response)
            FacesContext.getCurrentInstance()
                    .addMessage(null,
                            new FacesMessage(FacesMessage.SEVERITY_INFO,"Saved Successfully",null));
        return navigateToPage();
    }
    public String update(){
        bookSubGroupDataModelJsf.getBookSubGroupDto().setUpdatedByAdminDto(adminDto);
        var response = bookSubGroupService.updateBookSubGroup(bookSubGroupDataModelJsf.getBookSubGroupDto());
        if (response)
            FacesContext.getCurrentInstance()
                    .addMessage(null,
                            new FacesMessage(FacesMessage.SEVERITY_INFO,"Updated Successfully",null));
        return navigateToPage();
    }
    public String saveUpdate(){
        return bookSubGroupDataModelJsf.getBookSubGroupDto().getId() == null ?save():update();
    }

    public String delete(){
        bookSubGroupDataModelJsf.getBookSubGroupDto().setDeletedByAdminDto(adminDto);
        var response = bookSubGroupService.deleteBookSubGroup(bookSubGroupDataModelJsf.getBookSubGroupDto());
        if (response)
            FacesContext.getCurrentInstance()
                    .addMessage(null,
                            new FacesMessage(FacesMessage.SEVERITY_WARN,"Deleted Successfully",null));
        return navigateToPage();
    }
public void loadBookGroupDropDownMenu(){
        bookSubGroupDataModelJsf.setBookGroupDtoListForDropDown(bookSubGroupService.findBookGroupsForDropDown());
}

    public String initCreate(){
        bookSubGroupDataModelJsf.setBookSubGroupDto(new BookSubGroupDto());
        loadBookGroupDropDownMenu();
        bookSubGroupDataModelJsf.setInitEdit(true);
        return returnToPage();
    }
    public String returnToPage(){
        return "bookSubGroup.xhtml?faces-redirect=true";
    }
    public String navigateToPage(){
        Utility.removeSessionBeanJSFDataModelObject("bookSubGroupDataModelJsf");
        bookSubGroupDataModelJsf = (BookSubGroupDataModelJsf) Utility.getSessionObject("bookSubGroupDataModelJsf");
        bookSubGroupDataModelJsf.setBookSubGroupDtoList(bookSubGroupService.findAllBookSubGroup());
         return returnToPage();
    }
    public String initEdit(){
        bookSubGroupDataModelJsf.setInitEdit(true);
        return returnToPage();
    }
}
