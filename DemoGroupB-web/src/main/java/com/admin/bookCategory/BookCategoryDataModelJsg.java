package com.admin.bookCategory;

import com.admin.dto.BookDto;
import lombok.Getter;
import lombok.Setter;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import java.io.Serializable;
import java.util.List;
import java.util.Optional;

@Getter
@Setter
@ManagedBean
@SessionScoped
public class BookCategoryDataModelJsg implements Serializable {
    private BookDto bookDto;
    private boolean isEdit;
    private List<BookDto> bookDtoList;

    public BookDto getBookDto(){
        return Optional.ofNullable(bookDto).orElse(bookDto = new BookDto());
    }
}
