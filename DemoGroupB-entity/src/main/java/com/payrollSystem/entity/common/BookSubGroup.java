package com.payrollSystem.entity.common;

import com.payrollSystem.entity.abstracts.AbstractProfile;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Collection;
import java.util.Optional;

@Getter
@Setter
@Entity
@Table(name = "BOOK_SUB_GROUP")
public class BookSubGroup extends AbstractProfile {

    @JoinColumn(name = "BOOKGROUP",nullable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private BookGroup bookGroup;

}
