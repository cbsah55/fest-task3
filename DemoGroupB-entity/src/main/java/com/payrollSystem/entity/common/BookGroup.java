package com.payrollSystem.entity.common;

import com.payrollSystem.entity.abstracts.AbstractProfile;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Table;

@Getter
@Setter
@Entity
@Table(name = "BOOK_GROUP")
public class BookGroup extends AbstractProfile {
}
